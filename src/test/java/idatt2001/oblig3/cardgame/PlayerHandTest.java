package idatt2001.oblig3.cardgame;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class PlayerHandTest {

    @Test
    @DisplayName("Sum of the faces")
    void sumOfTheFaces() {
        PlayerHand playerHand = new PlayerHand();
        ArrayList<PlayingCard> playingCards = new ArrayList<>();
        playingCards.add(new PlayingCard('S', 2));
        playingCards.add(new PlayingCard('D', 4));
        playingCards.add(new PlayingCard('H', 6));
        playingCards.add(new PlayingCard('H', 8));
        playingCards.add(new PlayingCard('H', 10));
        playingCards.add(new PlayingCard('H', 12));
        playerHand.addCards(playingCards);

        int sum = playerHand.sumOfTheFaces();
        assertEquals(sum, 42);

    }

    @Test
    @DisplayName("Cords of heart")
    void cordsOfHeart() {
        PlayerHand playerHand = new PlayerHand();
        ArrayList<PlayingCard> playingCards = new ArrayList<>();
        playingCards.add(new PlayingCard('S', 2));
        playingCards.add(new PlayingCard('D', 4));
        playingCards.add(new PlayingCard('H', 6));
        playingCards.add(new PlayingCard('H', 8));
        playingCards.add(new PlayingCard('D', 10));
        playingCards.add(new PlayingCard('S', 12));
        playerHand.addCards(playingCards);

        assertEquals(playerHand.cordsOfHeart(), "H6H8");
    }

    @Test
    @DisplayName("Is flush")
    void isFlush() {
        PlayerHand playerHand = new PlayerHand();
        ArrayList<PlayingCard> playingCards = new ArrayList<>();
        playingCards.add(new PlayingCard('S', 2));
        playingCards.add(new PlayingCard('H', 4));
        playingCards.add(new PlayingCard('H', 6));
        playingCards.add(new PlayingCard('H', 8));
        playingCards.add(new PlayingCard('H', 10));
        playingCards.add(new PlayingCard('H', 12));
        playerHand.addCards(playingCards);

        assertEquals(playerHand.isFlush(), "Yes");
    }

    @Test
    @DisplayName("Is not flush")
    void isNotFlush() {
        PlayerHand playerHand = new PlayerHand();
        ArrayList<PlayingCard> playingCards = new ArrayList<>();
        playingCards.add(new PlayingCard('S', 2));
        playingCards.add(new PlayingCard('D', 4));
        playingCards.add(new PlayingCard('H', 6));
        playingCards.add(new PlayingCard('H', 8));
        playingCards.add(new PlayingCard('H', 10));
        playingCards.add(new PlayingCard('H', 12));
        playerHand.addCards(playingCards);

        assertEquals(playerHand.isFlush(), "No");
    }

    @Test
    @DisplayName("Is queen of spades")
    void isQueenOfSpades() {
        PlayerHand playerHand = new PlayerHand();
        ArrayList<PlayingCard> playingCards = new ArrayList<>();
        playingCards.add(new PlayingCard('S', 12));

        playerHand.addCards(playingCards);

        assertEquals(playerHand.isQueenOfSpades(), "Yes");
    }

    @Test
    @DisplayName("Is not queen of spades")
    void isNotQueenOfSpades() {
        PlayerHand playerHand = new PlayerHand();
        ArrayList<PlayingCard> playingCards = new ArrayList<>();
        playingCards.add(new PlayingCard('S', 2));

        playerHand.addCards(playingCards);

        assertEquals(playerHand.isQueenOfSpades(), "No");
    }
}