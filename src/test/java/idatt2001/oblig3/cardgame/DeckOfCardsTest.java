package idatt2001.oblig3.cardgame;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

    @Test
    @DisplayName("Deal hand gives correct amount of cards")
    void dealHandGivesCorrectAmountOfCards() {
        DeckOfCards deckOfCards = new DeckOfCards();
        int cardsRecieved = deckOfCards.dealHand(8).size();
        assertEquals(cardsRecieved, 8);
    }

    @Test
    @DisplayName("Deal hand too many cards")
    void dealHandTooManyCards() {
        DeckOfCards deckOfCards = new DeckOfCards();
        assertThrows(Exception.class, () -> deckOfCards.dealHand(80));

    }
}