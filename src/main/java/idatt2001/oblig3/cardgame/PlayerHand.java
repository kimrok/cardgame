package idatt2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * PlayerHand.java
 * Represents the hand of cards of a player and the methods checking the hand
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0 2021.04.05
 * @since 2021.04.05
 */
public class PlayerHand {
    private ArrayList<PlayingCard> hand;

    public PlayerHand() {
        hand = new ArrayList<>();
    }

    /**
     * Adds Plaing Cards from the deck and calls for the hand to be sorted
     *
     * @param cards
     */
    public void addCards(ArrayList<PlayingCard> cards) {
        hand.addAll(cards);
        sortHand();
    }

    /**
     * @return sum of the faces on cards in playerhand
     */
    public int sumOfTheFaces() {
        //Error if empty
        if (hand.size() != 0) {
            return hand.stream().map(PlayingCard::getFace).reduce((a, b) -> a + b).get();
        }
        return 0;
    }

    /**
     * Makes a string with all the cords of hearts
     * Made as String because i am VERY lazy. Please return ArrayList next time!
     *
     * @return String of all cords of heart
     */
    public String cordsOfHeart() {
        List<PlayingCard> cordsOfHeart;
        if (hand.stream().anyMatch(card -> card.getSuit()=='H')) {
            cordsOfHeart = hand.stream()
                    .filter(card -> card.getSuit()=='H')
                    .collect(Collectors.toList());
            return cordsOfHeart.stream().map(e -> e.getAsString()).reduce("", (s, str) -> s.concat(str + " "));
        }
        return "No Hearts";
    }

    /**
     * Checks if the cards in hand make a flush (5 cards of same color)
     * Made with Yes or No because i am VERY lazy. Please do boolean next time!
     *
     * @return String of Yes or No
     */
    public String isFlush() {
        if (
                hand.stream().filter(card -> String.valueOf(card.getSuit()).equals("S")).count() >= 5 ||
                hand.stream().filter(card -> String.valueOf(card.getSuit()).equals("H")).count() >= 5 ||
                hand.stream().filter(card -> String.valueOf(card.getSuit()).equals("D")).count() >= 5 ||
                hand.stream().filter(card -> String.valueOf(card.getSuit()).equals("C")).count() >= 5
        ) {
            return "Yes";
        }
        return "No";
    }

    /**
     * Checks if the queen of spades is in the hand.
     * Made with Yes or No because i am VERY lazy. Please do boolean next time!
     *
     * @return String of Yes or No
     */
    public String isQueenOfSpades() {
        if (hand.stream().anyMatch(card -> card.getAsString().equals("S12"))) {
            return "Yes";
        }
        return "No";
    }

    /**
     * Makes a String with all the cards.
     * Made as String because i am VERY lazy. Please return ArrayList next time!
     *
     * @return All cards in the hand
     */
    public String allCards() {
        StringBuilder allCards = new StringBuilder();
        for (PlayingCard card:hand) {
            allCards.append(card.getAsString()).append(" ");
        }
        return allCards.toString();
    }

    /**
     * Sorts the player hand, so that all suits are together and sorted by faces
     */
    private void sortHand() {
        hand.sort((a, b) -> a.getAsString().compareTo(b.getAsString()));
    }
}
