package idatt2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Random;

/**
 * DeckOfCards.java
 * Represents a whole deck of 52 PlayingCards used in card games
 *
 * @author Kim Johnstuen Rokling
 * @version 1.1 2021.03.30
 * @since 2021.03.29
 */
public class DeckOfCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private ArrayList<PlayingCard> playingCards = new ArrayList<>(52);

    /**
     * Constructor generates an array of PlayingCards numbering 1 to 13,
     * each with a face S, H, D or C. Generates a total of 52 unique cards.
     */
    public DeckOfCards() {
        for (char c : suit) {
            for (int j = 1; j < 14; j++) {
                playingCards.add(new PlayingCard(c, j));
            }
        }
    }

    /**
     * Takes n amount of cards away from the playingCards and gives them away.
     *
     * @param n
     * @return ArrayList with cards dealt
     */
    public ArrayList<PlayingCard> dealHand(int n) {
        if (playingCards.size() < n) {
            n = playingCards.size();
        }
        ArrayList<PlayingCard> dealHand = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int cardNumber = randomNumber();
            dealHand.add(playingCards.get(cardNumber));
            playingCards.remove(cardNumber);
        }
        return dealHand;
    }

    /**
     * @return integer from 0 to the size of the card deck
     */
    private int randomNumber() {
        Random random = new Random();
        return random.nextInt(playingCards.size());
    }
}
