package idatt2001.oblig3.cardgame;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 * Client.java
 *
 * @author Kim Johnstuen Rokling
 * @version 1.1 2021.04.05
 * @since 2021.03.29
 */
public class Client extends Application {
    DeckOfCards deckOfCards = new DeckOfCards();
    PlayerHand playerHand = new PlayerHand();

    Stage window;
    Text sumFacesText;
    Text cordsHeartText;
    Text flushText;
    Text queenOfSpadesText;
    Text cards;

    @Override
    public void start(Stage stage) throws Exception {
        window = stage;
        window.setTitle("Poker Game");

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(40, 40, 40, 40));
        gridPane.setVgap(6);
        gridPane.setHgap(10);

        //Button deal hand
        Button dealHandButton = new Button("Deal hand");
        gridPane.setConstraints(dealHandButton, 6, 3);
        dealHandButton.setMinSize(80, 30);
        dealHandButton.setOnAction(e -> {
            playerHand.addCards(deckOfCards.dealHand(5));
            cards.setText(playerHand.allCards());
        });

        //Button check hand
        Button checkHandButton = new Button("Check hand");
        gridPane.setConstraints(checkHandButton, 6, 5);
        checkHandButton.setMinSize(80, 30);
        checkHandButton.setOnAction(e -> {
            sumFacesText.setText(String.valueOf(playerHand.sumOfTheFaces()));
            cordsHeartText.setText(playerHand.cordsOfHeart());
            flushText.setText(playerHand.isFlush());
            queenOfSpadesText.setText(playerHand.isQueenOfSpades());
        });

        //Sum of faces label and text
        Label sumFacesLabel = new Label("Sum of the faces:");
        gridPane.setConstraints(sumFacesLabel, 0, 9);
        sumFacesLabel.setMinSize(100, 30);
        sumFacesText = new Text("0");
        gridPane.setConstraints(sumFacesText, 1, 9);

        //Cords of heart label and text
        Label cordsHeartLabel = new Label("Cords of hearts:");
        gridPane.setConstraints(cordsHeartLabel, 2, 9);
        cordsHeartLabel.setMinSize(100, 30);
        cordsHeartText = new Text("No Hearts");
        cordsHeartText.setWrappingWidth(100);
        gridPane.setConstraints(cordsHeartText, 3, 9);

        //Flush label and text
        Label flushLabel = new Label("Flush:");
        gridPane.setConstraints(flushLabel, 0, 10);
        flushText = new Text("No");
        gridPane.setConstraints(flushText, 1, 10);

        //Queen of spades label and text
        Label queenOfSpadesLabel = new Label("Queen of spades:");
        gridPane.setConstraints(queenOfSpadesLabel, 2, 10);
        queenOfSpadesText = new Text("No");
        gridPane.setConstraints(queenOfSpadesText, 3, 10);

        //Players cards
        cards = new Text("No cards");
        cards.setWrappingWidth(320);
        gridPane.setConstraints(cards, 0, 0, 6, 8);

        gridPane.getChildren().addAll(
                dealHandButton, checkHandButton, sumFacesLabel, sumFacesText, cordsHeartLabel,
                cordsHeartText, flushLabel, flushText, queenOfSpadesLabel, queenOfSpadesText, cards
        );
        Scene scene = new Scene(gridPane, 550, 250);
        window.setScene(scene);
        window.show();
    }

    @Override
    public void init() throws Exception {
        super.init();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
